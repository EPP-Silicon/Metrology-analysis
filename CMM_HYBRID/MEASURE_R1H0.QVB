'Author: Paarangat Pushkarna
'Created: 15/02/2023
'Purpose: Execute R1H0 hybrid measurement

'HYBRID_BASE_CLASS.QVH declares the HybPts type and labelling function
'R1H0_CONFIGS.QVH declares a subroutine to specify values relevant to R1H0 hybrid
'When config header file is called, base class header file also called

'$Include: "R1H0_CONFIGS.QVH"

Sub Main
   
   Dim R1H0 As HybPts 'Specs of R1H0 stored in the R1H0 struct/record
   Dim pt_names$() 'Array of Str to store labels for R1H0
   Dim calibrations() 'Array to store calibration pt coords
   Dim positions() 'Array to store position pt coords
   Dim glue_locs() 'Array to store glueing pt coords
   
   'Assigns values to arrays declared above with constants specified in R1H0_CONFIGS.QVH
   Call configure_R1H0(R1H0, pt_names$(), calibrations(), positions(), glue_locs())
   
   
   'Output stream
   
   Open "HYBRID_R1H0.txt" For Output As #1
   Write #1, "Location," + "X [mm]," + "Y [mm]," + "Z [mm]"
   
   Dim i As Integer
   Dim start As Integer
   Dim fin As Integer
   
   ' Lighting conditions for sensor 
    Light.SetAll   Coax:=0.02, Ring:=0, Stage:=0    
    
    ' Measure calibration points
    
    Dim FocusBuffer As Double
    
    start = 0
    fin = R1H0.numcal - 1
    For i = start To fin
    
        ' Opens a measurement block and clears the point buffer to begin measuring a new point feature. Assigns label.     
        Measure.Point
        
        ' Move to the following XYZ coordinates
        Stage.MoveTo calibrations(i,0), calibrations(i,1), 0  
        FocusTool.SetMode   FocusType:=SURFACE, Speed:=SLOW, Range:=10
        
        ' Runs tool and adds a point to the point buffer
        FocusTool.Run calibrations(i,0), calibrations(i,1),0 
        
        'The Measure.EndMeas method will fit a point to the points in the point buffer, create a new point feature in the database, and return its ID. 
        Measure.EndMeas 
        
        'This method reports feature measurement results in the Measurement Results window. 
        Results.ReportFeature Show:=X_ And Y_ And Z_, Tag:=0
        Results.Print 'As Boolean
        
        'Write to file 
        'The and here is concatennating , the feature db is picking out the x,y,z from the coordinate in the database 
        Write #1, pt_names$(i) & "," & FeatureDB.Item.X.Actual & "," & FeatureDB.Item.Y.Actual & "," & FeatureDB.Item.Z.Actual
            
    Next i
    

    'Need to set the light level to be saturated so that there is sufficient contrast on 
    'parts of the hybrid tangential to the fiducial markers. Due to the tolerance, 
    'the coordinate navigated to may not be directly on the marker.  
    Light.SetAll   Coax:=0.14, Ring:=0, Stage:=0  
    
    ' Hybrid fiducial positions measured manually after  being navigated to
    Dim refidx As Integer  
    start = fin + 1
    fin = fin + R1H0.numpos
    For i = start To fin
    
        refidx = i - start
        ' Opens a measurement block and clears the point buffer to begin measuring a new point feature. Assigns label.     
        Measure.Point
        ' Move to the following XYZ coordinates
        
        Stage.MoveTo positions(refidx,0), positions(refidx,1), -0.3
        FocusTool.Focus positions(refidx,0), positions(refidx,1),-0.45
        
        ManualTool.Run   
        'The Measure.EndMeas method will fit a point to the points in the point buffer, create a new point feature in the database, and return its ID. 
        Measure.EndMeas
        ManualTool.Reset
        'This method reports feature measurement results in the Measurement Results window. 
        Results.ReportFeature Show:=X_ And Y_ And Z_, Tag:=0
        Results.Print 'As Boolean
        'Write to file 
        'The and here is concatennating , the feature db is picking out the x,y,z from the coordinate in the database 
        Write #1, pt_names$(i) & "," & FeatureDB.Item.X.Actual & "," & FeatureDB.Item.Y.Actual & "," & FeatureDB.Item.Z.Actual 
            
    Next i
      
    'It is better to have a reference hybrid height as interpolating the height becomes
    'more consistently achievable. 
    Dim HybridZCoord As Double
    HybridZCoord = -0.45
    
    start = fin + 1
    fin = fin + R1H0.glupts
    
    For i = start To fin
        refidx = i - start
        
        ' Opens a measurement block and clears the point buffer to begin measuring a new point feature. Assigns label.     
        Measure.Point
        
        ' Move to the following XYZ coordinates
        Stage.MoveTo glue_locs(refidx,0), glue_locs(refidx,1), HybridZCoord 
        
        'Semi-automated measurement
        'Focus without adding point to the point buffer
        'User nominated point is added to the point buffer        
        'FocusTool.Focus glue_locs(refidx,0), glue_locs(refidx,1), HybridZCoord
        'ManualTool.Run

        'Automated measurement
        ' Runs tool and adds a point to the point buffer
        FocusTool.SetMode   FocusType:=SURFACE, Speed:=SLOW, Range:=10
        FocusTool.Run glue_locs(refidx,0), glue_locs(refidx,1), HybridZCoord 
        

        'The Measure.EndMeas method will fit a point to the points in the point buffer, create a new point feature in the database, and return its ID. 
        Measure.EndMeas 
        'This method reports feature measurement results in the Measurement Results window. 
        
        Results.ReportFeature Show:=X_ And Y_ And Z_, Tag:=0
        Results.Print 'As Boolean 
        
        'Write to file 
        'The and here is concatennating , the feature db is picking out the x,y,z from the coordinate in the database
        Write #1, pt_names$(i) & "," & FeatureDB.Item.X.Actual & "," & FeatureDB.Item.Y.Actual & "," & FeatureDB.Item.Z.Actual 
            
    Next i
    
    Close #1
   
End Sub
