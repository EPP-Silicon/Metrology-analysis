# Metrology software guide

*Please open this `.md` file in a browser for best viewing.*

# CMM (Coordinate Measurement Machine) stage

# Python stage

This section details operating procedure for generation of ITk database 
compliant `.json` and `.txt` files for metrology of R1 and R4 modules.

This stage takes `.txt` files written by the CMM (Coordinate Measuring Machine)
as input, performs Quality Control tasks detailed by ITk documentation, and 
writes results of QC tasks to `.json` and `.txt` files according to ITk spec.

This stage assumes that `.txt` file outputs from the CMM, have already been 
generated. 

## Usage walkthrough
### Collecting CMM output files

### Running python scripts

### Outputs


