"""
Tony Tran, 2022
tonytransasc@gmail.com

CMM bowing data processor and plotter.

Concave tolerance is 150 um.
Convex tolerance is 50 um.

The coordinate system is defined such that below the corners of the sensor,
z is positive.

Bowing is defined to be the difference in height between the
highest point and the lowest point.

Class
-----
    BowingAnalysis()
Methods
-------
    unique()
    bowing_data_filter()
    calculate_bowing()
Functions
---------
    write_to_file()
Imported functions
------------------
    file_processor()
    plot_3d()
"""

from os import path
import datetime
import numpy as np

import geometry_config as cfg
from common_functions import file_processor, plot_3d
from cmm_data_formatter import (
    HYBRID_TYPE,
    INSTITUTE,
    INSTRUMENT_TYPE,
    MEASUREMENT_PROGRAM_VERSION,
    MODULE_REF_NUMBER,
    MODULE_TYPE,
    OPERATOR_NAME,
    RUN_NUMBER,
    DataFormatter,
)
from pull_env_from_db import queryDB

# CMM output file, txt format. Input file of this script. Contains grid point
# scan measurements.
BOWING_INPUT_FILE_PATH = "bowing_analysis_input_files/"
BOWING_INPUT_FILE = (
    "melb_r1_bowing_proto_2_run2.txt"  # input("Name of input file is:")
)
INPUT_PATH_AND_FILE = path.relpath(BOWING_INPUT_FILE_PATH + BOWING_INPUT_FILE)

# Output file of this script. Contains grip point scan with repeated points and
# points off the sensor filtered out.
BOWING_OUTPUT_FILE_PATH = "bowing_analysis_output_files/"
BOWING_OUTPUT_TXT_FILE = (
    "bowing_scan_output.txt"  # input("Name of output txt file is:")
)
JSON_OUTPUT_FILE = BOWING_INPUT_FILE.replace(".txt", "_output_json.JSON")
BOWING_OUTPUT_PLOT = "bowing_R0_sensor.png"
OUTPUT_PATH_AND_FILE = path.relpath(BOWING_OUTPUT_FILE_PATH + BOWING_OUTPUT_TXT_FILE)
JSON_PATH_AND_FILE = path.relpath(BOWING_OUTPUT_FILE_PATH + JSON_OUTPUT_FILE)
OUTPUT_PATH_AND_PLOT = path.relpath(BOWING_OUTPUT_FILE_PATH + BOWING_OUTPUT_PLOT)

# TXT file header inputs
MODULE_TYPE = "EC"
HYBRID_TYPE = "ML"  # input("Hybrid type: ")
MODULE_REF_NUMBER = "123456789"  # input("Module ref number: ")
DATE = str(datetime.datetime.now())
INSTITUTE = "UoM"
OPERATOR_NAME = "Tony Tran"  # input("Operator name: ")
INSTRUMENT_TYPE = "Quick Vision Pro"
RUN_NUMBER = str(1)  # input("Run number: ")
MEASUREMENT_PROGRAM_VERSION = "v1"  # input("Measurement program version: ")
SCRIPT_VERSION = "1"
# Variables to write to JSON file.
pass_var = input("Passed? (Y/N)")
if pass_var == "Y":
    PASSED = True
elif pass_var == "N":
    PASSED = False
else:
    raise Exception("Invalid input.")
prob_var = input("Problems? (Y/N)")
if prob_var == "Y":
    PROBLEMS = True
elif prob_var == "N":
    PROBLEMS = False
else:
    raise Exception("Invalid input.")

class BowingAnalysis:
    """Class representing a collection of points in 3-D space.

    Methods clean and analyses data.

    Attributes:
    -----------
        data : []
            A 3 column array of CMM X,Y,Z measurements.
        data_unique : []
            Data with duplicates removed.
        data_filtered : []
            Data with points on the hybrid and jig removed.
    Methods:
    --------
        unique()
           Removes duplicates in data set.
        bowing_data_filter()
            Removes points that are not on the sensor.
        calculate_bowing()
            Calculate bowing and print curvature.
    """

    def __init__(self, input_path_and_file):
        """Constructs instance using data file.

        Parameters
        ----------
        input_path_and_file : str
            Used to open a file and store contents in array.
        """
        self.data = file_processor(input_path_and_file)
        self.data_unique = [[], [], []]
        self.data_filtered = [[], [], []]
        # Measurement
        self.bowing = 0

    def unique(self):
        """Removes duplicates in data set.

        When the CMM fails to focus on a point, it prints out the previous point
        in the output file. This function removes those duplicates.
        """
        # A 3 row array of CMM X, Y, Z measurements
        data_transpose = np.transpose(self.data)
        # Remove duplicate points
        self.data_unique[2], idx = np.unique(data_transpose[2], return_index=True)
        self.data_unique[0] = data_transpose[0][idx]
        self.data_unique[1] = data_transpose[1][idx]

    def bowing_data_filter(self):

        """Removes points that are not on the sensor.

        The grid scan covers the hybrid and parts of the jig. To get the bowing,
        only want points on the sensor.
        """

        for i, _ in enumerate(self.data_unique[0]):
            if cfg.Z_BOUND_LOWER < self.data_unique[2][i] < cfg.Z_BOUND_UPPER:

                self.data_filtered[0].append(self.data_unique[0][i])
                self.data_filtered[1].append(self.data_unique[1][i])
                self.data_filtered[2].append(self.data_unique[2][i])

    def calculate_bowing(self):
        """Calculate bowing and print curvature."""

        self.bowing = np.multiply(
            np.max(self.data_filtered[2]) - np.min(self.data_filtered[2]), 1000
        )
        # Bowing for negative z is larger
        if np.abs(np.max(self.data_filtered[2])) < np.abs(
            np.min(self.data_filtered[2])
        ):
            curvature = "concave"
        # Bowing for positive z is larger
        elif np.abs(np.max(self.data_filtered[2])) > np.abs(
            np.min(self.data_filtered[2])
        ):
            curvature = "convex"
        print("Bowing = " + str(self.bowing) + " um , " + curvature + ".")
        # Concave tolerance is 150 um
        if curvature == "concave" and self.bowing > 150:
            print("Too much bowing.")
        # Convex tolerance is 50 um
        elif curvature == "convex" and self.bowing > 50:
            print("Too much bowing.")
        else:
            print("ERROR")


def write_to_file(output_path_and_file, x_array, y_array, z_array):
    """Writes array to file.

    Header: "X,Y,Z"
    Lines: "#,#,#"

    The points are ordered from lowest z to highest.

    Parameters
    ----------
    output_path_and_file : str
        _description_
    x_array : []
        _description_
    y_array : []]
        _description_
    z_array : []]
        _description_
    """

    with open(output_path_and_file, "w", encoding="utf-8") as file:
        file.write("X , Y , Z")
        file.write("\n")
        for i, _ in enumerate(x_array):
            file.write(str(x_array[i]) + "," + str(y_array[i]) + "," + str(z_array[i]))
            file.write("\n")


def main():
    """Takes in output CMM file. Writes filtered .txt file and plots the results."""

    sensor = BowingAnalysis(INPUT_PATH_AND_FILE)
    sensor.unique()
    sensor.bowing_data_filter()
    sensor.calculate_bowing()

    sensor_name = "UNIT-01"
    temperature, humidity = queryDB(sensor_name)
    print(f"Temperature for this measurement: {temperature}")
    print(f"Humidity for this measurement: {humidity}")

    write_to_file(
        OUTPUT_PATH_AND_FILE,
        sensor.data_filtered[0],
        sensor.data_filtered[1],
        sensor.data_filtered[2],
    )
    # plot_title = input("Plot title:")
    plot_3d(
        OUTPUT_PATH_AND_PLOT,
        sensor.data_filtered[0],
        sensor.data_filtered[1],
        np.multiply(sensor.data_filtered[2], 1000),
        title="CMM grid scan",  # plot_title
        x_label="X (mm)",
        y_label="Y (mm)",
        z_label="Z (um)",
    )
    # Write results to file according to CMM documentation
    formatted_data_file = DataFormatter(
        "B", np.transpose(sensor.data_filtered), OUTPUT_PATH_AND_FILE
    )
    formatted_data_file.construct_header(
        MODULE_TYPE,
        HYBRID_TYPE,
        MODULE_REF_NUMBER,
        DATE,
        INSTITUTE,
        OPERATOR_NAME,
        INSTRUMENT_TYPE,
        RUN_NUMBER,
        MEASUREMENT_PROGRAM_VERSION,
    )
    formatted_data_file.write_bow_measurements()
    json_file = DataFormatter("B", np.transpose(sensor.data_filtered), JSON_PATH_AND_FILE)
    json_file.json_header(
        module_ref_number= MODULE_REF_NUMBER,
        institution= INSTITUTE,
        passed=PASSED,
        problems=PROBLEMS,
        instrument_type=INSTRUMENT_TYPE,
        operator= OPERATOR_NAME,
        script_version= SCRIPT_VERSION,
        run_number=RUN_NUMBER,
        test_type= "MODULE_BOW",
    )
    json_file.json_body_bow(
        sensor.bowing
    )
    json_file.write_json_file(JSON_PATH_AND_FILE)
    print(json_file.dict)
if __name__ == "__main__":
    main()
