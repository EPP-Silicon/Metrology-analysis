"""
Author: Paarangat Pushkarna
Date Created: 22/02/2023
CMM data preparation for future QC analysis.
"""
from pathlib import Path
import pandas as pd
import numpy as np
import argparse

from metrology_constants import offsets

def setup_cli_parser() -> argparse.ArgumentParser:
    """Setup the command line interaction, help message, and inputs."""

    desc = "Data cleaning of CMM output for further processing."
    parser = argparse.ArgumentParser(desc)

    iodir_desc = "Input directory containing CMM output txt files."
    parser.add_argument(
        '-i',
        '--iodir',
        type = str,
        help = iodir_desc,
        metavar='',
        required=True
    )

    return parser.parse_args()

def format_cmm_line(unformatted_string: str) -> str:
    """Format a line of raw string input from the CMM output text files.

    Removes all whitespace and "" string marking characters. Splits by ","
    delimiter.
    """
    return unformatted_string.rstrip()\
            .replace(" ", "").replace('"', "")\
            .split(",")

def make_dataframe_from_txt(txt_location: Path) -> pd.DataFrame:
    """Creates pd.DataFrame from CMM output text file.

    output:
        pd.DataFrame(columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location"])
    """
    with open(txt_location, "r") as thand:

        header = format_cmm_line(next(thand))
        data = [format_cmm_line(line) for line in thand]
        strarr = np.array(data)
        labels = strarr[:, 0]
        strpts = strarr[:, 1:]
        fltpts = strpts.astype(np.float64)

        df = pd.DataFrame(data = fltpts, columns = header[1:])
        df[header[0]] = labels

    return df

def calibrate_heights(df: pd.DataFrame) -> pd.DataFrame:
    """DEPRECATED 22/06/2023: 
    Subtracts average of calibration measurements from heights.

    input:
        pd.DataFrame(columns =
            ["X[mm]", "Y[mm]", "Z[mm]", "Location"])
    output:
        pd.DataFrame(columns =
            ["X[mm]", "Y[mm]", "Z[mm]", "Location", "Z_CAL[mm]"])

    Discards calibration points after processing.
    This is because balibration points from different parts can cause confusion
    if present in the same pd.DataFrame together.
    Deprecated because use not exactly specified. Was initially a "legacy". 
    """

    caldf = df.loc[df["Location"].str.contains("CALIB")]
    mncalz = np.mean(caldf["Z[mm]"])
    df["Z_CAL[mm]"] = df["Z[mm]"] - mncalz

    return df.loc[~(df["Location"].str.contains("CALIB"))]

def make_module_dataframe(iodir: Path) -> pd.DataFrame:
    """Combine CMM text files (raw data) into one pd.DataFrame

    output: pd.DataFrame(
    columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location"]
    )

    Opens directory storing CMM text files, parses files into pd.DataFrames.
    Subtracts average of calibration points from height measurements.
    Cats all dataframes together for combined analysis.
    """

    all_text_files = list(iodir.glob("*.txt"))
    calibrated_dfs = []
    for text_file in all_text_files:
        if ("OUT" in text_file.stem):
            continue #skip over txt file output that is already present
        component_df = make_dataframe_from_txt(text_file)
        calibrated_dfs.append(component_df)
    fulldf = pd.concat(calibrated_dfs)

    return fulldf.loc[~(fulldf["Location"].str.contains("CALIB"))]

def infer_module_type(df: pd.DataFrame) -> str:
    """Infer the module (R1 or R4) from naming convention in txt files.

    input:
        df = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])
    output:
        stub = str

    Use the location column to infer the type of module being used.
    """
    elt_to_pick = 0 #just pick one location name from list and use it to infer
    stubs_bool = df["Location"].str.contains("R")
    stubs = df.loc[stubs_bool, "Location"].to_list()[elt_to_pick].split("_")
    module_type = stubs[1][:2] #return the "R1" or "R4" part of the string
    # print(f"Module type inferred: {module_type}")

    return module_type

def apply_position_offset(df: pd.DataFrame) -> pd.DataFrame:
    """Apply offset to X,Y positions so that measurements agree with collab.

    input:
        df = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])
    outputs:
        df_with_offset = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])

    Origin assigned as the outer edge of guardring on R1 and R4 sensors.
    Collaboration uses actual sensor corner as origin.
    X,Y positions w.r.t guardring transformed to X,Y positions w.r.t corner.
    Achieved by applying a fixed offset.
    """

    module_type = infer_module_type(df)

    x_offset, y_offset = offsets[module_type]
    df["X[mm]"] += x_offset
    df["Y[mm]"] += y_offset

    return df

def invert_z_axis(df: pd.DataFrame) -> pd.DataFrame:
    """Invert z axis to agree with the QC documentation and collaboration.

    input:
        df = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])
    outputs:
        df = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])

    Since our CMM has z-axis being positive into the page, we need to invert
    the z-axis by multiplying by -1 on all the calibrated coordinates.
    """

    df["Z[mm]"] = df["Z[mm]"] * (-1)

    return df

def make_dataframe_qc_ready(iodir: Path) -> pd.DataFrame:
    """Collect data from CMM output directory, and prepare for QC analysis.

    input:
        Path object of input/output directory (iodir)
    output:
        df = pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])

    Performs initial data cleaning operations, so that future analyses are
    compliant with most current QC documentation.
    - Collects data from CMM output directory
    - Applies Z coordinate calibration on each component
    - Puts measurements for each component into one dataframe
    - Offsets (x, y) positions by the guardring offset
    - Inverts the z-axis
    """

    calibrated_module_df = make_module_dataframe(iodir)
    qc_ready_module_df = apply_position_offset(
        invert_z_axis(calibrated_module_df))

    return qc_ready_module_df

def main():
    """If this script is run as main the cleaned data will be printed to screen.
    This is to assist in debugging.
    """

    cli = setup_cli_parser()
    df = make_dataframe_qc_ready(Path(cli.iodir))
    print(df.to_string())

    return

if __name__ == "__main__":
    main()
