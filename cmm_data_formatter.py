#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : Tony Tran
# Created Date: Oct 2022
# =============================================================================
"""
This module formats the data output from the CMM as specified within the QC document.

The output of this module is a .txt file containing formmated raw measurements and
a JSON file containing the output 

This module is intended to be imported in to the various analysis scripts, thereby
streamlining the production of database compliant data files. As required,
output files maintain the raw data output of the CMM.

In addition, this works as a standalone script that takes in a raw data file and
outputs a formatted txt file.

Class
-----
    DataFormatter()
Methods
-------
    construct_header()
    write_bow_measurements()
    sensor_selector()
    funnel_data()
    shape_data()
    write_position_measurements()
    write_calibration_and_glue_measurements()
    write_stackup_measurements()
Functions
---------
    seperate_data()
"""
from os import path
import json
import datetime
import numpy as np
from common_functions import file_processor
import geometry_config as cfg

now = datetime.datetime.now()

# CMM output file, txt format. Input file of this script.
INPUT_PATH = "bowing_analysis_input_files/"
INPUT_FILE = "R0_sensor_vacuumed_21_by_21.txt"  # input("Name of input file is:")
INPUT_PATH_AND_FILE = path.relpath(INPUT_PATH + INPUT_FILE)
# Output file of this script. CMM results formatted according to QC doc.
OUTPUT_PATH = "formatted_data_files/"
OUTPUT_NAME = "formatted_bowing_data_test.txt"
OUTPUT_PATH_AND_FILE = path.relpath(OUTPUT_PATH + OUTPUT_NAME)
JSON_NAME = "json_file.JSON"
JSON_PATH_AND_FILE = path.relpath(OUTPUT_PATH + JSON_NAME)

# Input data to be written to file
raw_data = file_processor(INPUT_PATH_AND_FILE)

test_data = np.arange(
    cfg.NO_OF_CALIBRATION_MEASUREMENTS
    + 6
    + cfg.NO_OF_R1H0_RAW_MEASUREMENTS
    + cfg.NO_OF_R1H1_RAW_MEASUREMENTS
    + 23
)
raw_data_T = [test_data, test_data, test_data]
raw_data = np.transpose(raw_data_T)
# Header arguments
MODULE_TYPE = "EC"
HYBRID_TYPE = "ML"  # input("Hybrid type: ")
MODULE_REF_NUMBER = "123456789"  # input("Module ref number: ")
DATE = str(now)
INSTITUTE = "UoM"
OPERATOR_NAME = "Tony Tran"  # input("Operator name: ")
INSTRUMENT_TYPE = "Quick Vision Pro"  # input("Instrument type: ")
RUN_NUMBER = str(1)  # input("Run number: ")
MEASUREMENT_PROGRAM_VERSION = "v1"  # input("Measurement program version: ")


def seperate_data(
    data: "list",
    index_a: "int",
    index_b: "int",
    index_c: "int",
    index_d: "int",
    index_e: "int",
) -> "list":
    """This function seperates a given array according to the indices specified.

    Within this module, this function is used to seperate the calibration data, the
    position data, the r(1/4)h0 data, the r(1/4)h1 data, and the (r1/r4) powerboard
    glue data, and the stack-up data.

    Parameters
    ----------
    data : []
    index_a : int
    index_b : int
    index_c : int
    index_d : int

    Returns
    -------
    _type_
        _description_
    """
    array_1 = data[:index_a]
    array_2 = data[index_a : index_a + index_b]
    array_3 = data[index_a + index_b : index_a + index_b + index_c]
    array_4 = data[index_a + index_b + index_c : index_a + index_b + index_c + index_d]
    array_5 = data[
        index_a
        + index_b
        + index_c
        + index_d : index_a
        + index_b
        + index_c
        + index_d
        + index_e
    ]
    array_6 = data[index_a + index_b + index_c + index_d + index_e :]

    return array_1, array_2, array_3, array_4, array_5, array_6


class DataFormatter:
    """Formats raw data output from the CMM in compliance with the database.

    Methods
    -------
        construct_header()
        write_bow_measurements()
        sensor_selector()
        funnel_data()
        shape_data()
        write_position_measurements()
        write_calibration_and_glue_measurements()
        write_stackup_measurements()
    """

    def __init__(self, bowing_or_pcb, data, output_path_and_file):
        # Arguments of instance
        self.data = data
        self.file = open(output_path_and_file, "w", encoding="utf-8")
        if bowing_or_pcb == "B":
            # Bowing scan does not require further attributes
            self.dict = {
                "component": "20USBML1234615",
                "institution": "MELB",
                "passed": True,
                "problems": False,
                "properties": {
                    "JIG": "some_string",
                    "OPERATOR": "some_string",
                    "USED_SETUP": "some_string",
                    "SCRIPT_VERSION": "some_string",
                },
                "results": {"BOW": 0},
                "runNumber": "1",
                "testType": "some_string",
            }
        elif bowing_or_pcb == "P":
            # Data seperated in to respective arrays for ease of writing.
            self.calibration_data = []
            self.position_data = []
            self.hybrid_1_data = []
            self.hybrid_2_data = []
            self.pb_glue_data = []
            self.pb_stackup_data = []
            # Data then shaped for ease of writing
            self.glue_data = []
            # Locations to be written
            self.position_locations = []
            self.glue_locations = []
            self.stack_up_locations = []
            self.cap_locations = []
            # Initialise dictionary of results
            self.dict = {
                "component": "20USBML1234615",
                "institution": "MELB",
                "passed": True,
                "problems": False,
                "properties": {
                    "MACHINE": "SmartScope Flash 250",
                    "OPERATOR": "Matthew Gignac",
                    "SCRIPT_VERSION": "some_string"
                },
                "results": {
                    "CAP_HEIGHT": {},
                    "FILE": "",
                    "HYBRID_GLUE_THICKNESS": {},
                    "HYBRID_POSITION": {},
                    "PB_GLUE_THICKNESS": {},
                    "PB_POSITION": {},
                    "SHIELDBOX_HEIGHT": 0,
                },
                "runNumber": "1",
                "testType": "MODULE_METROLOGY",
            }
        else:
            raise Exception("Invalid scan (B/P)")

    def construct_header(
        self,
        module_type,
        hybrid_type,
        module_ref_number,
        date,
        institute,
        operator_name,
        instrument_type,
        run_number,
        measurement_program_version,
    ):
        """
        Write the header as required by the QC documentation.

        Require the versatility of importing this module, hence it is more
        convenient to have the header details as input arguments,
        rather than local variables.
        """

        self.file.write("#--Header \n")
        self.file.write(f'{"EC or Barrel:":<30}{module_type:<30}\n')
        self.file.write(f'{"Hybrid type:":<30}{hybrid_type:<30}\n')
        self.file.write(f'{"Module ref. Number":<30}{module_ref_number:<30}\n')
        self.file.write(f'{"Date:":<30}{date:<30}\n')
        self.file.write(f'{"Institute:":<30}{institute:<30}\n')
        self.file.write(f'{"Operator:":<30}{operator_name:<30}\n')
        self.file.write(f'{"Instrument Type:":<30}{instrument_type:<30}\n')
        self.file.write(f'{"Run Number:":<30}{run_number:<30}\n')
        self.file.write(
            f'{"Measurement program version:":<30}{measurement_program_version:<30}\n'
        )

    def write_bow_measurements(self):
        """
        Write bowing measurements as required by the QC documentation.
        """
        # Want 3 rows of X,Y,Z data
        data_transpose = np.transpose(self.data)
        # Write header
        self.file.write("#---Bow\n")
        self.file.write(f'{"Location":<30}{"X[mm]":<30}{"Y[mm]":<30}{"Z[mm]":<30}\n')
        # Write results
        for i, _ in enumerate(data_transpose[0]):
            self.file.write(
                f'{"Sensor":<30}'
                f"{str(data_transpose[0][i]):<30}"
                f"{str(data_transpose[1][i]):<30}"
                f"{str(data_transpose[2][i]):<30}\n"
            )

    def sensor_selector(self):
        """Slice class initialisation data according to sensor typer and assign to
        respective attributes. Shape for ease of writing to file.
        """
        sensor_type = input("sensor_type (R1/R4): ")
        # Assign data to object attributes
        if sensor_type == "R1":
            (
                self.calibration_data,
                self.position_data,
                self.hybrid_1_data,
                self.hybrid_2_data,
                self.pb_glue_data,
                self.pb_stackup_data,
            ) = seperate_data(
                self.data,
                cfg.NO_OF_CALIBRATION_MEASUREMENTS,
                3 * cfg.NO_OF_POSITION_MEASUREMENTS,
                cfg.NO_OF_R1H0_RAW_MEASUREMENTS,
                cfg.NO_OF_R1H1_RAW_MEASUREMENTS,
                np.sum(cfg.R1_pb["number_of_glue_measurements"]),
            )
            # Stack glue data of respective hybrids and pb
            self.glue_data = np.vstack(
                [self.hybrid_1_data, self.hybrid_2_data, self.pb_glue_data]
            )

            self.position_locations = np.append(
                np.append(
                    cfg.R1H0_dict["position_locations"],
                    cfg.R1H1_dict["position_locations"],
                ),
                cfg.R1_pb["position_locations"],
            )

            self.glue_locations = cfg.R1_raw_glue_locations
            self.stack_up_locations = cfg.R1_pb["stack_up_measurement_locations"]
            self.cap_locations = cfg.R1_pb["capacitor_locations"]
        elif sensor_type == "R4":
            (
                self.calibration_data,
                self.position_data,
                self.hybrid_1_data,
                self.hybrid_2_data,
                self.pb_glue_data,
                self.pb_stackup_data,
            ) = seperate_data(
                self.data,
                cfg.NO_OF_CALIBRATION_MEASUREMENTS,
                3 * cfg.NO_OF_POSITION_MEASUREMENTS,
                cfg.NO_OF_R4H0_RAW_MEASUREMENTS,
                cfg.NO_OF_R4H1_RAW_MEASUREMENTS,
                np.sum(cfg.R4_pb["number_of_glue_measurements"]),
            )
            # Stack glue data of respective hybrids and pb
            self.glue_data = np.vstack(
                [self.hybrid_1_data, self.hybrid_2_data, self.pb_glue_data]
            )
            self.position_locations = np.append(
                np.append(
                    cfg.R4H0_dict["position_locations"],
                    cfg.R4H1_dict["position_locations"],
                ),
                cfg.R4_pb["position_locations"],
            )
            self.glue_locations = cfg.R4_raw_glue_locations
            self.stack_up_locations = cfg.R4_pb["stack_up_measurement_locations"]
            self.cap_locations = cfg.R4_pb["capacitor_locations"]

        else:
            raise Exception("Input sensor must be R1 or R4")

    def funnel_data(
        self,
        sensor,
        data,
        calibration_data,
        position_data,
        hybrid_1_data,
        hybrid_2_data,
        pb_glue_data,
        pb_stackup_data,
    ):
        """Reassign object attributes.

        Function to be used alternatively to sensor_selector() when imported.
        sensor_selector() is more useful when using this module as a standalone script.
        This provides the utility of using parameters from a seperate module.
        """
        self.data = data
        self.calibration_data = calibration_data
        self.position_data = position_data
        self.hybrid_1_data = hybrid_1_data
        self.hybrid_2_data = hybrid_2_data
        self.pb_glue_data = pb_glue_data
        self.pb_stackup_data = pb_stackup_data

        self.glue_data = np.vstack(
            [self.hybrid_1_data, self.hybrid_2_data, self.pb_glue_data]
        )
        if sensor == "R1":
            self.position_locations = np.append(
                np.append(
                    cfg.R1H0_dict["position_locations"],
                    cfg.R1H1_dict["position_locations"],
                ),
                cfg.R1_pb["position_locations"],
            )

            self.glue_locations = cfg.R1_raw_glue_locations
            self.stack_up_locations = cfg.R1_pb["stack_up_measurement_locations"]
            self.cap_locations = cfg.R1_pb["capacitor_locations"]
        elif sensor == "R4":
            self.position_locations = np.append(
                np.append(
                    cfg.R4H0_dict["position_locations"],
                    cfg.R4H1_dict["position_locations"],
                ),
                cfg.R4_pb["position_locations"],
            )
            self.glue_locations = cfg.R4_raw_glue_locations
            self.stack_up_locations = cfg.R4_pb["stack_up_measurement_locations"]
            self.cap_locations = cfg.R4_pb["capacitor_locations"]
        else:
            raise Exception("Input sensor must be R1 or R4")

    def shape_data(self):
        """Shapes data from columns in to rows before writing."""
        self.calibration_data = np.transpose(self.calibration_data)
        self.position_data = np.transpose(self.position_data)
        self.glue_data = np.transpose(self.glue_data)
        self.pb_stackup_data = np.transpose(self.pb_stackup_data)

    def write_position_measurements(self):
        """Write position data."""
        # Header
        self.file.write("---Positions:\n")
        self.file.write(f'{"Location":<30}{"X[mm]":<30}{"Y[mm]":<30}\n')
        # Position data
        for i, _ in enumerate(range(cfg.NO_OF_POSITION_MEASUREMENTS * 3)):
            self.file.write(
                f"{str(self.position_locations[i]):<30}"
                f"{str(self.position_data[0][i]):<30}"
                f"{str(self.position_data[1][i]):<30}\n"
            )

    def write_calibration_and_glue_measurements(self):
        """Write calibration data and glue data."""
        # Glue header
        self.file.write("---Glue Heights: \n")
        self.file.write(
            f'{"Location":<30}{"Type":<30}{"X[mm]":<30}{"Y[mm]":<30}{"Z[mm]":<30}\n'
        )
        # Calibration measurements
        for i, _ in enumerate(range(cfg.NO_OF_CALIBRATION_MEASUREMENTS)):
            self.file.write(
                f'{"Sensor":<30}{"1":<30}'
                f"{str(self.calibration_data[0][i]):<30}"
                f"{str(self.calibration_data[1][i]):<30}"
                f"{str(self.calibration_data[2][i]):<30}\n"
            )

        # Glue measurements:
        for i, _ in enumerate(
            range(len(self.glue_data[0]))
            # range(
            #     cfg.NO_OF_R4H0_RAW_MEASUREMENTS
            #     + cfg.NO_OF_R4H1_RAW_MEASUREMENTS
            #     + np.sum(cfg.R4_dict["number_of_glue_measurements"])
            # )
        ):
            self.file.write(
                f'{str(self.glue_locations[i]):<30}{"2":<30}'
                f"{str(self.glue_data[0][i]):<30}"
                f"{str(self.glue_data[1][i]):<30}"
                f"{str(self.glue_data[2][i]):<30}\n"
            )

    def write_stackup_measurements(self):
        """Write stackup data."""
        self.file.write("---Other heights:\n")
        self.file.write(
            f'{"Location":<30}{"Type":<30}{"X[mm]":<30}{"Y[mm]":<30}{"Z[mm]":<30}\n'
        )

        for i, _ in enumerate(self.stack_up_locations):
            self.file.write(
                f"{self.stack_up_locations[i]:<30}"
                f'{"4":<30}{str(self.pb_stackup_data[0][i]):<30}'
                f"{str(self.pb_stackup_data[1][i]):<30}"
                f"{str(self.pb_stackup_data[2][i]):<30}\n"
            )

    def json_header(
        self,
        module_ref_number,
        institution,
        passed,
        problems,
        instrument_type,
        operator,
        script_version,
        run_number,
        test_type,
    ):
        """Assign arguments to header header keys."""
        self.dict["component"] = module_ref_number
        self.dict["institution"] = institution
        self.dict["passed"] = passed
        self.dict["problems"] = problems
        self.dict["properties"]["MACHINE"] = instrument_type
        self.dict["properties"]["USED_SETUP"] = instrument_type
        self.dict["properties"]["OPERATOR"] = operator
        self.dict["properties"]["SCRIPT_VERSION"] = script_version
        self.dict["runNumber"] = run_number
        self.dict["testType"] = test_type

    def sort_data(self):
        """Parse processed data and assign it to attributes so that filling up the
        dictionary is more streamlined.

        Data is to be sorted in to:
        -Capacitor heights
        -Hybrid glue thicknesses x 2
        -Hybrid positions x 2
        -PB glue thicknesses
        -PB positions
        -Shieldbox height

        In addition to the processed values, also need a list of location names.
        """

    def json_body_h_and_pb(
        self,
        file,
        sensor,
        cap_heights,
        h_1_pos,
        h_2_pos,
        pb_pos,
        proc_hybrid_1_glue_data,
        proc_hybrid_2_glue_data,
        proc_pb_glue_data,
        proc_pb_shield_data,
    ):
        """Want to take in attributes of instances of the hybrid and pb objects, then
        fill up the dictionary.

        Don't need to funnel in measurements that do not require procecssing, ie positions and caps.
        """
        self.dict["results"]["FILE"] = file

        if sensor == "R1":
            for i, value in enumerate(cap_heights):
                print(self.cap_locations)
                print(self.cap_locations[i], value)
                self.dict["results"]["CAP_HEIGHT"].update(
                    {self.cap_locations[i]: value}
                )
            _ = [
                self.dict["results"]["CAP_HEIGHT"].update(
                    {self.cap_locations[i]: value}
                )
                for i, value in enumerate(cap_heights)
            ]
            for i, value in enumerate(proc_hybrid_1_glue_data):
                self.dict["results"]["HYBRID_GLUE_THICKNESS"].update(
                    {cfg.R1H0_dict["measurement_locations"][i]: value}
                )
            for i, value in enumerate(proc_hybrid_2_glue_data):
                self.dict["results"]["HYBRID_GLUE_THICKNESS"].update(
                    {cfg.R1H1_dict["measurement_locations"][i]: value}
                )
            for i, value in enumerate(proc_pb_glue_data):
                self.dict["results"]["PB_GLUE_THICKNESS"].update(
                    {cfg.R1_pb["glue_measurement_locations"][i]: value}
                )

        elif sensor == "R4":
            for i, value in enumerate(proc_hybrid_1_glue_data):
                self.dict["results"]["HYBRID_GLUE_THICKNESS"].update(
                    {cfg.R4H0_dict["measurement_locations"][i]: value}
                )
            for i, value in enumerate(proc_hybrid_2_glue_data):
                self.dict["results"]["HYBRID_GLUE_THICKNESS"].update(
                    {cfg.R4H1_dict["measurement_locations"][i]: value}
                )
            for i, value in enumerate(proc_pb_glue_data):
                self.dict["results"]["PB_GLUE_THICKNESS"].update(
                    {cfg.R4_pb["glue_measurement_locations"][i]: value}
                )

        else:
            raise Exception("Input sensor must be R1 or R4")
        _ = [
            self.dict["results"]["HYBRID_POSITION"].update(
                {self.position_locations[i]: value}
            )
            for i, value in enumerate(h_1_pos)
        ]
        _ = [
            self.dict["results"]["HYBRID_POSITION"].update(
                {self.position_locations[2 + i]: value}
            )
            for i, value in enumerate(h_2_pos)
        ]
        _ = [
            self.dict["results"]["PB_POSITION"].update(
                {self.position_locations[4 + i]: value}
            )
            for i, value in enumerate(pb_pos)
        ]
        self.dict["results"]["SHIELDBOX_HEIGHT"] = proc_pb_shield_data

    def json_body_bow(self, bow_value):
        """Fill in results with bow value."""
        # Want 3 rows of X,Y,Z data
        self.dict["results"]["BOW"] = bow_value

    def write_json_file(self, output_path_and_name):
        """_summary_

        _extended_summary_
        """
        with open(output_path_and_name, "w", encoding="utf-8") as outfile:
            json.dump(self.dict, outfile, indent=4)


def main():
    """Format and write input bowing / pcb data to file."""
    selection = input("Bowing or PCB scan? (B/P)")
    data = DataFormatter(selection, raw_data, OUTPUT_PATH_AND_FILE)
    data.construct_header(
        MODULE_TYPE,
        HYBRID_TYPE,
        MODULE_REF_NUMBER,
        DATE,
        INSTITUTE,
        OPERATOR_NAME,
        INSTRUMENT_TYPE,
        RUN_NUMBER,
        MEASUREMENT_PROGRAM_VERSION,
    )
    if selection == "B":
        # Write txt file
        data.write_bow_measurements()
        # Write JSON file
        data.json_header(
            module_ref_number="20USBML1234615",
            institution="MELB",
            passed=True,
            problems=False,
            instrument_type="Mitutoya QV Pro",
            operator="Tony Tran",
            script_version= "1",
            run_number="1",
            test_type="MODULE_BOW",
        )
        data.json_body_bow(777)
        data.write_json_file(JSON_PATH_AND_FILE)
    elif selection == "P":
        # Write txt file
        data.sensor_selector()
        data.shape_data()
        data.write_position_measurements()
        data.write_calibration_and_glue_measurements()
        data.write_stackup_measurements()
        # Write JSON file
        data.json_header(
            module_ref_number="20USBML1234615",
            institution="MELB",
            passed=True,
            problems=False,
            instrument_type="Mitutoya QV Pro",
            operator="Tony Tran",
            script_version="1",
            run_number="1",
            test_type="MODULE_METROLOGY",
        )
        data.json_body_h_and_pb(
            file="some_string",
            sensor="R1",
            cap_heights=[0, 0, 0],
            h_1_pos=[[0, 0], [0, 0]],
            h_2_pos=[[0, 0], [0, 0]],
            pb_pos=[[0, 0], [0, 0]],
            proc_hybrid_1_glue_data=[0, 0, 0],
            proc_hybrid_2_glue_data=[0, 0, 0],
            proc_pb_glue_data=[0, 0, 0],
            proc_pb_shield_data=0,
        )
        data.write_json_file(JSON_PATH_AND_FILE)
    else:
        raise Exception("Invalid user input.")


if __name__ == "__main__":
    main()
