""" Common functions used in metrology, imported in to analysis modules.

The functions here are used in both the analysis of hybrids and powerboards.

Functions
---------
    file_processor()
    plot_2d()
    plot_3d()


"""
import matplotlib.pyplot as plt
import numpy as np
import geometry_config as cfg


def file_processor(input_path: "str") -> "list":
    """Remove the header and store the contents of a CMM text file in to an array.

    Parameters
    ----------
    input_file : str, path to file
        Input CMM file looks like:
        "X,Y,Z"
        "#,#,#"
        .
        .
        .

    Returns
    -------
    list
        The output array is shaped as:
        [[#, #, # ],
        [#, #, # ],
        [#, #, # ],
        .
        .
        .
        ]
    """
    with open(input_path, "r", encoding="utf-8") as a_file:
        _ = next(a_file)  # header_line
        data_lines = list(a_file)
    # Convert string to float. Store the floats in to a matrix
    data = []
    for line in data_lines:
        these_values = line.replace("\n", "").replace('"', "").split(",")
        these_values = [float(i) for i in these_values]
        data.append(these_values)
    return data

def hybrid_glue_plot(
        output_path_and_file_name,
        x_data_array,
        *y_data_arrays,
        title,
        x_label,
        y_label
    ):
    """Make a 2D plot with the input x and y data.

    A general 2d plotting function.
    However, this is to be used for CMM point measurement plots. All measurements have
    equal intrinsic uncertainty, hence measurement error can be taken to be
    constant.

    Parameters
    ----------
    x_data_array : []
        Names of measurement locations.
    y_data_arrays : []
        Height measurements. Possibly multiple.
    title : str
    x_label : str
    y_label : str
    """

    _, axes = plt.subplots()
    axes.set_title(title)
    axes.set_xlabel(x_label)
    axes.set_ylabel(y_label)
    _, labels = plt.xticks()
    plt.setp(labels, rotation=45)
    # Data
    for y_data_array in y_data_arrays:
        axes.scatter(x_data_array, y_data_array, color="black", marker="o")
        plt.errorbar(x_data_array, y_data_array, yerr=10, fmt="o", color="black")
    # Plot glue tolerances
    upper_tolerance = np.zeros(len(x_data_array))
    upper_tolerance += 1000 * (
        cfg.NOMINAL_HYBRID_SENSOR_GLUE_HEIGHT
        + cfg.HYBRID_SENSOR_GLUE_HEIGHT_TOLERANCE
    )
    lower_tolerance = np.zeros(
        len(x_data_array[:-4])
    )  # Last 4 measurements have a lower bound of 0.
    lower_tolerance += 1000 * (
        cfg.NOMINAL_HYBRID_SENSOR_GLUE_HEIGHT
        - cfg.HYBRID_SENSOR_GLUE_HEIGHT_TOLERANCE
    )
    axes.plot(x_data_array, upper_tolerance, color="red")
    axes.plot(x_data_array[:-4], lower_tolerance, color="red")
    # Intrinsic CMM focussing error is 10 um
    axes.grid(which="both")
    # axes.legend()
    plt.savefig(output_path_and_file_name, bbox_inches="tight", dpi=250)
    plt.show()



def xy_position_plot(
    output_path_and_file_name, x_data_array, *y_data_arrays, title, x_label, y_label
):
    """Produces plot of hybrid / pb fiducial alignment measurements.

    Parameters
    ----------
    x_data_array : []
    x coords of alignment measurements.
    y_data_array : []
        y coords of alignment measurements.
    title : str
    x_label : str
    y_label : str
    """
    _, axes = plt.subplots()
    axes.set_title(title)
    axes.set_xlabel(x_label)
    axes.set_ylabel(y_label)
    _, labels = plt.xticks()
    plt.setp(labels, rotation=45)
    for y_data_array in y_data_arrays:
        axes.scatter(x_data_array, y_data_array, color="black", marker="o")
        plt.errorbar(
            x_data_array, y_data_array, xerr=10, yerr=10, fmt="o", color="black"
        )
    axes.grid(which="both")
    plt.savefig(output_path_and_file_name, bbox_inches="tight", dpi=250)
    plt.show()


def plot_3d(
    output_path_and_file_name, x_data, y_data, z_data, title, x_label, y_label, z_label
):
    """Produces a 3D scatter plot of the input data.

    With respect to CMM measurements, this is typically used for
    bowing measurements.

    Parameters
    ----------
    x_data : []
    y_data : []
    z_data : []
    title : str
    x_label : str
    y_label : str
    z_label : str
    """
    fig = plt.figure()
    axes = fig.add_subplot(111, projection="3d")
    axes.set_title(title)
    axes.set_xlabel(x_label)
    axes.set_ylabel(y_label)
    axes.set_zlabel(z_label)
    axes.scatter(
        x_data,
        y_data,
        z_data,
        s=30,
        marker="o",
        color="black",
        #cmap="Greens",
    )
    # Error bars not included due to visual clutter.
    plt.savefig(output_path_and_file_name, bbox_inches="tight", dpi=250)
    plt.show()


def write_3_arrays(output_file, array1, array2, array3):
    """Takes in 3 arrays and write them to a file.

    To be used for writing measurement object names, glue heights,
    and difference from nominal to a .txt file.
    """
    file = open(output_file, "w", encoding="utf-8")
    for i, _ in enumerate(array1):
        file.write(str(array1[i]) + "," + str(array2[i]) + "," + str(array3[i]))
        file.write("\n")
