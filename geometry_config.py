"""
Config file containing module parameters and dictionaries.

Variables and dictionaries imported in to analysis scripts
for convenience.

Values are stored in this configuration file as they are subject to change.
"""
import numpy as np

# Cutoff bounds for bowing scan
Z_BOUND_LOWER = -0.10  # mm
Z_BOUND_UPPER = 0.15  # mm
# CMM focussing uncertainty
CMM_FOCUSSING_UNCERTAINTY = 0.010  # mm
# Thicknesses and heights obtained from module drawings
HYBRID_THICKNESS = 0.270  # mm
POWERBOARD_THICKNESS = 0.300  # mm
PCB_UNCERTAINTY = 0.010  # mm
# Nominal height for r1 powerboard
POWER_PAD_HEIGHT = 0.3 + 0.12
CAPACITOR_HEIGHT = 3.24 - 0.32
SHIELD_HEIGHT = 5.84 - 0.32
# Nominal height for hybrids
HYBRID_PAD_HEIGHT = 0.3 + 0.12
NOMINAL_HYBRID_SENSOR_GLUE_HEIGHT = 0.12  # mm
HYBRID_SENSOR_GLUE_HEIGHT_TOLERANCE = 0.04  # mm
# no of raw point measurements
MEASUREMENTS_PER_CHIP = 4
MEASUREMENTS_PER_HCC = 3
# No of xy fiducial measurements. This is 2 for all hybrids and powerboards.
NO_OF_POSITION_MEASUREMENTS = 2
# Powerboard

R1_pb = {
    "no_of_powerpads": 5,
    "no_of_capacitors": 4,
    "no_of_shield_measurements": 5,
    "position_locations": ["PB_P1", "PB_P2"],
    "measurement_locations": [
        "PB_1",
        "PB_2",
        "PB_3",
        "PB_4",
        "PB_5",
        "C1",
        "C2",
        "C3",
        "C4",
        "SHIELDBOX_HEIGHT",
    ],
    "glue_measurement_locations": [
        "PB_1",
        "PB_2",
        "PB_3",
        "PB_4",
        "PB_5",
    ],
    "number_of_glue_measurements": [
        4,
        1,
        4,
        4,
        1,
    ],  # When the total number of glue measurements is required, used np.sum()
    "stack_up_measurement_locations": [
        "C1",
        "C2",
        "C3",
        "C4",
        "SHIELD",
        "SHIELD",
        "SHIELD",
        "SHIELD",
        "SHIELD",
    ],
    "capacitor_locations": ["C1", "C2", "C3", "C4"],
    "nominal_values": [  # Module drawings
        0.3 + 0.12,
        0.3 + 0.12,
        0.3 + 0.12,
        0.3 + 0.12,
        0.3 + 0.12,
        3.24 - 0.32,
        3.24 - 0.32,
        3.24 - 0.32,
        3.24 - 0.32,
        5.84 - 0.32,
    ],
}

R4_pb = {
    "no_of_powerpads": 5,
    "no_of_capacitors": 8,
    "no_of_shield_measurements": 5,
    "position_locations": ["PB_P1", "PB_P2"],
    "measurement_locations": [
        "PB_1",
        "PB_2",
        "PB_3",
        "PB_4",
        "PB_5",
        "C1",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "SHIELDBOX_HEIGHT",
    ],
    "glue_measurement_locations": [
        "PB_1",
        "PB_2",
        "PB_3",
        "PB_4",
        "PB_5",
    ],
    # no of point measurements for PB_1,PB_2,PB_3,PB_4,PB_5
    "number_of_glue_measurements": [4, 1, 4, 4, 1],
    "stack_up_measurement_locations": [
        "C1",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "SHIELD",
        "SHIELD",
        "SHIELD",
        "SHIELD",
        "SHIELD",
    ],
    "capacitor_locations": ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8"],
}
# Hybrids

R0H0_dict = {
    "no_of_chips": 8,
    "no_of_HCCs": 1,
    "no_of_HCC_point_measurements": 3,
    "no_of_bondpad_measurements": 3,
    "no_of_bondpad_point_measurements": [2, 4, 2],  # For H_RXHY_0, H_RXHY_1, H_RXHY_2
    "position_locations": ["H_R0H0_P1", "H_R0H0_P2"],
    "measurement_locations": [
        "ABC_R0H0_0",
        "ABC_R0H0_1",
        "ABC_R0H0_2",
        "ABC_R0H0_3",
        "ABC_R0H0_4",
        "ABC_R0H0_5",
        "ABC_R0H0_6",
        "ABC_R0H0_7",
        "HCC_R0H0_0",
        "H_R0H0_0",
        "H_R0H0_1",
        "H_R0H0_2",
    ],
}

R0H1_dict = {
    "no_of_chips": 9,
    "no_of_HCCs": 1,
    "no_of_HCC_point_measurements": 3,
    "no_of_bondpad_measurements": 3,
    "no_of_bondpad_point_measurements": [2, 4, 2],  # For H_RXHY_0, H_RXHY_1, H_RXHY_2
    "position_locations": ["H_R0H1_P1", "H_R0H1_P2"],
    "measurement_locations": [
        "ABC_R0H1_0",
        "ABC_R0H1_1",
        "ABC_R0H1_2",
        "ABC_R0H1_3",
        "ABC_R0H1_4",
        "ABC_R0H1_5",
        "ABC_R0H1_6",
        "ABC_R0H1_7",
        "ABC_R0H1_8",
        "HCC_R0H1_0",
        "H_R0H1_0",
        "H_R0H1_1",
        "H_R0H1_2",
    ],
}

R1H0_dict = {
    "no_of_chips": 10,
    "no_of_HCCs": 1,
    "no_of_HCC_point_measurements": 3,
    "no_of_bondpad_measurements": 3,
    "no_of_bondpad_point_measurements": [2, 4, 2],  # For H_RXHY_0, H_RXHY_1, H_RXHY_2
    "position_locations": ["H_R1H0_P1", "H_R1H0_P2"],
    "measurement_locations": [
        "ABC_R1H0_0",
        "ABC_R1H0_1",
        "ABC_R1H0_2",
        "ABC_R1H0_3",
        "ABC_R1H0_4",
        "ABC_R1H0_5",
        "ABC_R1H0_6",
        "ABC_R1H0_7",
        "ABC_R1H0_8",
        "ABC_R1H0_9",
        "HCC_R1H0_0",
        "H_R1H0_0",
        "H_R1H0_1",
        "H_R1H0_2",
    ],
}

R1H1_dict = {
    "no_of_chips": 11,
    "no_of_HCCs": 1,
    "no_of_HCC_point_measurements": 3,
    "no_of_bondpad_measurements": 3,
    "no_of_bondpad_point_measurements": [2, 4, 2],  # For H_RXHY_0, H_RXHY_1, H_RXHY_2
    "position_locations": ["H_R1H1_P1", "H_R1H1_P2"],
    "measurement_locations": [
        "ABC_R1H1_0",
        "ABC_R1H1_1",
        "ABC_R1H1_2",
        "ABC_R1H1_3",
        "ABC_R1H1_4",
        "ABC_R1H1_5",
        "ABC_R1H1_6",
        "ABC_R1H1_7",
        "ABC_R1H1_8",
        "ABC_R1H1_9",
        "ABC_R1H1_10",
        "HCC_R1H1_0",
        "H_R1H1_0",
        "H_R1H1_1",
        "H_R1H1_2",
    ],
}

R4H0_dict = {
    "no_of_chips": 8,
    "no_of_HCCs": 0,
    "no_of_HCC_point_measurements": 0,
    "no_of_bondpad_measurements": 3,
    "no_of_bondpad_point_measurements": [2, 4, 2],  # For H_RXHY_0, H_RXHY_1, H_RXHY_2
    "position_locations": ["H_R4H0_P1", "H_R4H0_P2"],
    "measurement_locations": [
        "ABC_R4H0_0",
        "ABC_R4H0_1",
        "ABC_R4H0_2",
        "ABC_R4H0_3",
        "ABC_R4H0_4",
        "ABC_R4H0_5",
        "ABC_R4H0_6",
        "ABC_R4H0_7",
        "H_R4H0_0",
        "H_R4H0_1",
        "H_R4H0_2",
    ],
}

R4H1_dict = {
    "no_of_chips": 8,
    "no_of_HCCs": 2,
    "no_of_HCC_point_measurements": 3,  # per chip
    "no_of_bondpad_measurements": 2,
    "no_of_bondpad_point_measurements": [2, 2],  # For H_RXHY_0, H_RXHY_1
    "position_locations": ["H_R4H1_P1", "H_R4H1_P2"],
    "measurement_locations": [
        "ABC_R4H1_0",
        "ABC_R4H1_1",
        "ABC_R4H1_2",
        "ABC_R4H1_3",
        "ABC_R4H1_4",
        "ABC_R4H1_5",
        "ABC_R4H1_6",
        "ABC_R4H1_7",
        "HCC_R4H1_0",
        "HCC_R4H1_1",
        "H_R4H1_0",
        "H_R4H1_1",
    ],
}
# Number of measurements
NO_OF_CALIBRATION_MEASUREMENTS = 4

NO_OF_R1H0_RAW_MEASUREMENTS = (
    R1H0_dict["no_of_chips"] * MEASUREMENTS_PER_CHIP
    + R1H0_dict["no_of_HCCs"] * MEASUREMENTS_PER_HCC
    + np.sum(R1H0_dict["no_of_bondpad_point_measurements"])
)
NO_OF_R1H1_RAW_MEASUREMENTS = (
    R1H1_dict["no_of_chips"] * MEASUREMENTS_PER_CHIP
    + R1H1_dict["no_of_HCCs"] * MEASUREMENTS_PER_HCC
    + np.sum(R1H1_dict["no_of_bondpad_point_measurements"])
)
NO_OF_R4H0_RAW_MEASUREMENTS = (
    R4H0_dict["no_of_chips"] * MEASUREMENTS_PER_CHIP
    + R4H0_dict["no_of_HCCs"] * MEASUREMENTS_PER_HCC
    + np.sum(R4H0_dict["no_of_bondpad_point_measurements"])
)
NO_OF_R4H1_RAW_MEASUREMENTS = (
    R4H1_dict["no_of_chips"] * MEASUREMENTS_PER_CHIP
    + R4H1_dict["no_of_HCCs"] * MEASUREMENTS_PER_HCC
    + np.sum(R4H1_dict["no_of_bondpad_point_measurements"])
)

# List of measurement locations for raw measurements.
# Used for standard formatting of the raw data output.
R1_raw_glue_locations = []
j = 0
# Chips
for i, _ in enumerate(range(R1H0_dict["no_of_chips"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_CHIP)):
        R1_raw_glue_locations.append(R1H0_dict["measurement_locations"][j])
    j += 1
# HCCs
for i, _ in enumerate(range(R1H0_dict["no_of_HCCs"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_HCC)):
        R1_raw_glue_locations.append(R1H0_dict["measurement_locations"][j])
    j += 1
# H_RXHY_0
for i in range(R1H0_dict["no_of_bondpad_point_measurements"][0]):
    R1_raw_glue_locations.append(R1H0_dict["measurement_locations"][j])
j += 1
# H_RXHY_1
for i in range(R1H0_dict["no_of_bondpad_point_measurements"][1]):
    R1_raw_glue_locations.append(R1H0_dict["measurement_locations"][j])
j += 1
# H_RXHY_2
for i in range(R1H0_dict["no_of_bondpad_point_measurements"][2]):
    R1_raw_glue_locations.append(R1H0_dict["measurement_locations"][j])
j += 1
# Append locations for R1H1 hybrid
j = 0
# Chips
for i, _ in enumerate(range(R1H1_dict["no_of_chips"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_CHIP)):
        R1_raw_glue_locations.append(R1H1_dict["measurement_locations"][j])
    j += 1
# HCCs
for i, _ in enumerate(range(R1H1_dict["no_of_HCCs"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_HCC)):
        R1_raw_glue_locations.append(R1H1_dict["measurement_locations"][j])
    j += 1
# H_RXHY_0
for i in range(R1H1_dict["no_of_bondpad_point_measurements"][0]):
    R1_raw_glue_locations.append(R1H1_dict["measurement_locations"][j])
j += 1
# H_RXHY_1
for i in range(R1H1_dict["no_of_bondpad_point_measurements"][1]):
    R1_raw_glue_locations.append(R1H1_dict["measurement_locations"][j])
j += 1
# H_RXHY_2
for i in range(R1H1_dict["no_of_bondpad_point_measurements"][2]):
    R1_raw_glue_locations.append(R1H1_dict["measurement_locations"][j])
j += 1
# Append pb glue locations
R1_pb_glue_locations = []
for i, value in enumerate(R1_pb["number_of_glue_measurements"]):
    for _, _ in enumerate(range(value)):
        R1_pb_glue_locations.append(R1_pb["glue_measurement_locations"][i])
R1_raw_glue_locations = np.append(R1_raw_glue_locations, R1_pb_glue_locations)
# # Append locations for powerboard
# R1_glue_locations = np.append(R1_glue_locations, R1_dict["measurement_locations"][:5])

# # After this, there is the "Other heights"
# R1_other_locations = []
# for i in range(R1_dict["no_of_capacitors"]):
#     R1_other_locations.append("C" + str(i + 1))
# for i in range(R1_dict["no_of_shield_measurements"]):
#     R1_other_locations.append("SHIELD")

# Repeat for R4
R4_raw_glue_locations = []
j = 0
# Chips
for i, _ in enumerate(range(R4H0_dict["no_of_chips"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_CHIP)):
        R4_raw_glue_locations.append(R4H0_dict["measurement_locations"][j])
    j += 1
# HCCs
for i, _ in enumerate(range(R4H0_dict["no_of_HCCs"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_HCC)):
        R4_raw_glue_locations.append(R4H0_dict["measurement_locations"][j])
    j += 1
# H_RXHY_0
for i in range(R4H0_dict["no_of_bondpad_point_measurements"][0]):
    R4_raw_glue_locations.append(R4H0_dict["measurement_locations"][j])
j += 1
# H_RXHY_1
for i in range(R4H0_dict["no_of_bondpad_point_measurements"][1]):
    R4_raw_glue_locations.append(R4H0_dict["measurement_locations"][j])
j += 1
# H_RXHY_0
for i in range(R4H0_dict["no_of_bondpad_point_measurements"][2]):
    R4_raw_glue_locations.append(R4H0_dict["measurement_locations"][j])
j += 1
# Append locations for R4H1 hybrid
j = 0
# Chips
for i, _ in enumerate(range(R4H1_dict["no_of_chips"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_CHIP)):
        R4_raw_glue_locations.append(R4H1_dict["measurement_locations"][j])
    j += 1
# HCCs
for i, _ in enumerate(range(R4H1_dict["no_of_HCCs"])):
    for _, _ in enumerate(range(MEASUREMENTS_PER_HCC)):
        R4_raw_glue_locations.append(R4H1_dict["measurement_locations"][j])
    j += 1
# H_RXHY_0
for i in range(R4H1_dict["no_of_bondpad_point_measurements"][0]):
    R4_raw_glue_locations.append(R4H1_dict["measurement_locations"][j])
j += 1
# H_RXHY_1
for i in range(R4H1_dict["no_of_bondpad_point_measurements"][1]):
    R4_raw_glue_locations.append(R4H1_dict["measurement_locations"][j])
j += 1


R4_pb_glue_locations = []
for i, value in enumerate(R4_pb["number_of_glue_measurements"]):
    for _, _ in enumerate(range(value)):
        R4_pb_glue_locations.append(R4_pb["glue_measurement_locations"][i])
R4_raw_glue_locations = np.append(R4_raw_glue_locations, R4_pb_glue_locations)
