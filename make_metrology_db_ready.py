"""
Author: Paarangat Pushkarna
Date Created: 22/02/2023
Summary: Helper functions to assist with formatting per ITK requirements.
"""
from pathlib import Path
import json
import argparse

import numpy as np
import pandas as pd

from cmm_data_cleaning import make_dataframe_qc_ready, infer_module_type
from metrology_qc_functions import exec_qc_procedures

MM_TO_UM = 1000

def setup_cli_parser() -> argparse.ArgumentParser:
    """Setup the command line interaction, help message, and inputs."""

    desc = "Generation of output files to push to ITK database"
    parser = argparse.ArgumentParser(desc)

    iodir_desc = "Input directory containing CMM output txt files."
    parser.add_argument(
        '-i',
        '--iodir',
        type = str,
        help = iodir_desc,
        metavar='',
        required=True
    )

    outname_desc = "Output file name. See README for details on convetion."
    parser.add_argument(
        '-n', 
        '--outname', 
        type = str, help = outname_desc, 
        metavar='', 
        required=True
    )

    return parser.parse_args()

def mktype(locstr: str) -> int:
    """Assign a ITK 'type' to powerboard, sensor, hybrid measurements"""

    if ("HCC" in locstr) or ("ABC" in locstr):
        return 2
    if ("H_" in locstr) or ("PB_" in locstr):
        return 2
    if "CALIB" in locstr:
        return 1
    else:
        return 4

def write_header_to_text(fhand, header) -> None:

    fhand.write("#--Header\n")

    for key, value in header.items():
        if type(value) == dict:
            for key1, value1 in value.items():
                writestr = "{:<30}{:<30}\n".format(f'{key1}:', value1)
                fhand.write(writestr)
        else:
            writestr = "{:<30}{:<30}\n".format(f'{key}:', value)
            fhand.write(writestr)

    return

def write_position_to_text(fhand, df: pd.DataFrame) -> None:

    #cut dataframe, selecting powerboard and hybrid positions
    fhand.write("#--Positions\n")

    posn_rows = df["#Location"].str.contains(r"_P1|_P2")
    posn_df = df.loc[posn_rows, ["#Location", "X[mm]", "Y[mm]"]]
    posn_df.to_string(fhand,
                     header = True,
                     index = False,
                     formatters = {"#Location": "{:<15}".format,
                                   "X[mm]": "{:<15}".format,
                                   "Y[mm]": "{:<15}".format},
                     justify = "left")
    fhand.write("\n")

    return

def write_glue_to_text(fhand, df: pd.DataFrame) -> None:

    glue_labels = r"ABC_[a-zA-Z0-9]+_[0-9]+|HC*_[a-zA-Z0-9]+_[0-9]+|PB_[0-9]+"
    glue_rows = df["#Location"].str.contains(glue_labels)
    glue_df = df.loc[
    glue_rows, ["#Location", "Type", "X[mm]", "Y[mm]", "Z[mm]"]
    ]
    glue_df.to_string(fhand,
                        header = True,
                        index = False,
                        formatters = {"#Location": "{:<15}".format,
                                       "X[mm]": "{:<15}".format,
                                       "Y[mm]": "{:<15}".format,
                                       "Z[mm]": "{:<15}".format,
                                       "Type": "{:<5}".format},
                        justify = "left")
    fhand.write("\n")

    return

def write_aux_to_text(fhand, df: pd.DataFrame) -> None:

    #cut dataframe, select capacitor heights and shield heights
    fhand.write("#--Other heights\n")
    othbool = df["#Location"].str.contains(r"SHIELD|C[0-9]+")
    othdf = df.loc[
        othbool, ["#Location", "Type", "X[mm]", "Y[mm]", "Z[mm]"]]
    othdf.to_string(fhand,
                    header = True,
                    index = False,
                    formatters = {"#Location": "{:<15}".format,
                                   "X[mm]": "{:<15}".format,
                                   "Y[mm]": "{:<15}".format,
                                   "Z[mm]": "{:<15}".format,
                                   "Type": "{:<5}".format},
                    justify = "left")

    return

def make_text_file(outloc: Path, head_type: dict, df: pd.DataFrame) -> None:
    """Creates ITK output text file. Selects output order by 'type'.

    input:
    pd.DataFrame(
    columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location", "Z_CAL[mm]"]
    )

    Complying with ITK text file output order necessitates selecting specific
    rows from input pd.DataFrame.
    Selections made according to 'mktype' function.
    """

    df = df.rename(columns = {"Location": "#Location"})
    df.style.set_properties(**{'text-align':'left'})
    df["Type"] = df["#Location"].apply(mktype)

    with open(outloc, "w") as ohand:

        write_header_to_text(ohand, head_type)
        write_position_to_text(ohand, df)
        write_glue_to_text(ohand, df)
        write_aux_to_text(ohand, df)

    return

def extract_capacitor_heights(df: pd.DataFrame) -> dict:

    cap_rows = df["Location"].str.contains("C[0-9]+")
    cap_df = df.loc[cap_rows, ["Location", "Z[mm]"]]
    cap_df["Z[mm]"] = cap_df["Z[mm]"]*MM_TO_UM
    cap_df.rename(columns = {"Z[mm]":"CAP_HEIGHT"}, inplace=True)
    cap_df.set_index("Location", inplace=True)

    return cap_df.to_dict()

def extract_glue_thickness(df: pd.DataFrame, col_name: str) -> dict:

    glue_df = df.loc[:, ["GLUE_THICKNESS[mm]"]]
    glue_df["GLUE_THICKNESS[mm]"] = glue_df["GLUE_THICKNESS[mm]"]*MM_TO_UM
    glue_df.rename(columns = {"GLUE_THICKNESS[mm]": col_name}, inplace=True)

    return glue_df.to_dict()

def extract_shield_height(df: pd.DataFrame) -> dict:

    ret_df = df.rename(columns = {"Z_AGG[mm]": "SHIELDBOX_HEIGHT"})
    ret_df["SHIELDBOX_HEIGHT"] = ret_df["SHIELDBOX_HEIGHT"]*MM_TO_UM

    return ret_df.loc["SHIELD"].to_dict()

def extract_deviation(df: pd.DataFrame) -> dict:

    dev_df = df.loc[:, ["DELTA_X[mm]", "DELTA_Y[mm]"]]
    dev_df["DELTA_X[mm]"] = dev_df["DELTA_X[mm]"]*MM_TO_UM
    dev_df["DELTA_Y[mm]"] = dev_df["DELTA_Y[mm]"]*MM_TO_UM

    dev_dict = dev_df.T.to_dict(orient="list")

    hyb_dict = {}
    pb_dict = {}

    for k, v in dev_dict.items():
        if "H" in k:
            hyb_dict[k] = v
        if "PB" in k:
            pb_dict[k] = v

    ret_dict = {}
    ret_dict["HYBRID_POSITION"] = hyb_dict
    ret_dict["PB_POSITION"] = pb_dict

    return ret_dict

def create_metrology_dict(posn_df, hyb_df, pb_df, shield_df, qc_df) -> dict:

    metrology = {
        **extract_deviation(posn_df),
        **extract_glue_thickness(hyb_df, "HYBRID_GLUE_THICKNESS"),
        **extract_glue_thickness(pb_df, "PB_GLUE_THICKNESS"),
        **extract_shield_height(shield_df),
        **extract_capacitor_heights(qc_df)
    }

    return metrology

def assess_metrology(posn_df, hyb_df, pb_df) -> dict:

    assessment = {}
    assessment["passed"] = True
    assessment["problems"] = False

    posn_assessment = np.all(posn_df["PASSED?"].to_numpy())
    if not(posn_assessment):
        assessment["passed"] = False
        return assessment

    glue_df = pd.concat([hyb_df, pb_df])
    glue_fail = ("failed" in glue_df["RESULT"].to_list())
    glue_problem = ("passed with problems" in glue_df["RESULT"].to_list())

    if glue_fail:
        assessment["passed"] = False
        return assessment

    if glue_problem:
        assessment["passed"] = True
        assessment["problems"] = True
        return assessment

    return assessment

def make_json_file(qc_df: pd.DataFrame, qc_results: tuple, header: dict, outloc: Path) -> None:

    database_dict = {
    **assess_metrology(*qc_results[:-1]), #[:-1] means grab first 3 elts of tupl
    **header
    }
    database_dict["result"] = create_metrology_dict(*qc_results, qc_df)

    with open(outloc, "w") as fhand:
        json.dump(database_dict, fhand)


    return

def make_local_log_file(qc_results, logpath) -> None:

    with open(logpath, "w") as fhand:

        for df in qc_results:
            print(df.to_string(header=True, justify = "left"))
            df.to_string(fhand, header=True, justify = "left")
            fhand.write("\n")

    return

def cli_header() -> dict:
    """Prompt for user input to construct json dict"""

    header = {}
    header["institution"] = "MELB"
    header["testType"] = "MODULE_METROLOGY"
    header["properties"] = {"MACHINE": "Mitutoyo QuickVision Pro"}

    name_prompt = (
    "[OPERATOR] Please type first and last name (separated by space) of operator that conducted test, and press enter: "
    )
    header["properties"]["OPERATOR"] = input(name_prompt)

    # script_prompt = (
    #     "[SCRIPT_VERSION] Please type module metrology script version: "
    # ) DEPRECATED USER INPUT: Please change this hard-code if making 
    #substantial software changes.
    header["properties"]["SCRIPT_VERSION"] = 1

    comp_prompt = "[component] Please type ID number of component under test, and press enter: "
    header["component"] = input(comp_prompt)

    run_prompt = "[runNumber] Please type run number for component under test and press enter: "
    header["runNumber"] = input(run_prompt)

    return header

def main():

    cli = setup_cli_parser()
    header = cli_header()
    qc_ready_df = make_dataframe_qc_ready(Path(cli.iodir))
    qc_results = exec_qc_procedures(qc_ready_df)
    make_text_file(
        Path(cli.iodir)/f"OUT_{cli.outname}.txt",
        header,
        qc_ready_df
    )
    make_json_file(
        qc_ready_df,
        qc_results,
        header,
        Path(cli.iodir)/f"OUT_{cli.outname}.json"
    )

    return

if __name__ == "__main__":
    main()