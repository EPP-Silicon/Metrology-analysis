from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from metr_helpers import NOMINAL_HYBRID_SENSOR_HEIGHT, HYBRID_GLUE_TOLERANCE
MM_TO_UM = 1000

def hybrid_height_plot(plotloc: Path, plotname: str, df: pd.DataFrame) -> None:
    """Creates a plot of glue heights for hybrid points.

    input:
        pd.DataFrame(columns =
        ["Z_CAL_AGG[mm]", DELTA_NOMINAL[mm], WITHIN_TOL?]
        index = ["Location"])

    Uses only "Z_CAL_AGG[mm]" column of the input pd.DataFrame.
    Fiducial heights also included in plot (H_X_P1 etc.)
    """

    #Some regex because we need to select all hybrid points.
    hybrid_str = "[a-zA-Z]+_R[0-9]H[0-9]_[a-zA-Z0-9]+"
    hybrid_df = df.loc[df.index.str.contains(hybrid_str)]
    y_values = np.abs(hybrid_df["Z_CAL_AGG[mm]"].to_numpy())*MM_TO_UM
    x_values = hybrid_df.index.to_numpy()

    fig, ax = plt.subplots()
    plt.xticks(rotation = 90)
    ax.set_xlabel("Location")
    ax.set_ylabel(r"Glue height [$\mu$m]")
    ax.set_title("Hybrid-Sensor Glue heights")
    ax.scatter(x_values, y_values,
            color="black", marker = "o", label = "Glue heights")
    UPPER_TOL = MM_TO_UM*(NOMINAL_HYBRID_SENSOR_HEIGHT + HYBRID_GLUE_TOLERANCE)
    LOWER_TOL = MM_TO_UM*(NOMINAL_HYBRID_SENSOR_HEIGHT - HYBRID_GLUE_TOLERANCE)
    ax.axhline(UPPER_TOL, color = "red", label = "Glue height upper tol.")
    ax.axhline(LOWER_TOL, color = "blue", label = "Glue height lower tol.")
    ax.legend()
    ax.grid()
    fig.set_size_inches(9, 11.5)
    fig.savefig(plotloc/f"{plotname}_hybrid_glue.png")
    plt.show()


def pb_height_plot(plotloc: Path, plotname: str, df: pd.DataFrame) -> None:
    """Creates a plot of glue heights for powerboard points.

    input:
        pd.DataFrame(columns =
        ["Z_CAL_AGG[mm]", DELTA_NOMINAL[mm], WITHIN_TOL?]
        index = ["Location"])

    Uses only "Z_CAL_AGG[mm]" column of the input pd.DataFrame.
    Fiducial heights also included in plot (PB_P1 etc.)
    """

    #Some regex because we need to select all powerboard points.
    #No tolerances because powerboard tolerances not defined by Tony
    pb_str = "SHIELD|PB_[A-Z0-9]+|C[0-9]+"
    pb_df = df.loc[df.index.str.contains(pb_str)]
    y_values = np.abs(pb_df["Z_CAL_AGG[mm]"].to_numpy())*MM_TO_UM
    x_values = pb_df.index.to_numpy()

    fig, ax = plt.subplots()
    plt.xticks(rotation = 90)
    ax.set_xlabel("Location")
    ax.set_ylabel(r"Glue height [$\mu$m]")
    ax.set_title("Powerboard Glue heights")
    ax.scatter(x_values, y_values,
            color="black", marker = "o", label = "Glue heights")
    ax.grid()
    fig.savefig(plotloc/f"{plotname}_pb_glue.png")
    plt.show()

def hybrid_position_plot(plotloc: Path, plotname: str, df: pd.DataFrame) -> None:
    """Creates a plot of x,y positions of key hybrid points.

    input:
        pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])

    Fiducial points also included in plot (H_X_P1 etc.), but they do crowd a bit.
    """

    #Some regex because we need to select all hybrid points.
    hybrid_str = "[a-zA-Z]+_R[0-9]H[0-9]_[a-zA-Z0-9]+"
    hybrid_df = df.loc[df["Location"].str.contains(hybrid_str)]
    x_values = hybrid_df["X[mm]"].to_numpy()
    y_values = hybrid_df["Y[mm]"].to_numpy()
    labels = hybrid_df["Location"].to_numpy()

    fig, ax = plt.subplots()
    ax.set_xlabel("X[mm]")
    ax.set_ylabel("Y[mm]")
    ax.set_title("Hybrid Position measurements")
    ax.scatter(x_values, y_values, color = "black", marker = "o", s = 1)
    ax.grid()
    ax.invert_yaxis() # Consistency with PCS in specs.

    for i, txt in enumerate(labels):
        ax.annotate(txt, (x_values[i], y_values[i]),
                    fontsize = "xx-small", rotation = 45)

    fig.savefig(plotloc/f"{plotname}_hybrid_posn.png")
    plt.show()

def pb_position_plot(plotloc: Path, plotname: str, df: pd.DataFrame):
    """Creates a plot of x,y positions of key powerboard points.

    input:
        pd.DataFrame(columns =
        ["X[mm]", "Y[mm]", "Z[mm]", "Location"])

    Fiducial points also included in plot (PB_P1 etc.), but they do crowd a bit.
    """

    #Some regex to find the powerboard points.
    #shield or powerboard or capacitor.
    pb_str = "SHIELD|PB_[A-Z0-9]+|C[0-9]+"
    pb_bool = df["Location"].str.contains(pb_str)
    pb_df = df.loc[pb_bool]

    x_values = pb_df["X[mm]"].to_numpy()
    y_values = pb_df["Y[mm]"].to_numpy()
    labels = pb_df["Location"].to_numpy()

    fig, ax = plt.subplots()
    ax.set_xlabel("X[mm]")
    ax.set_ylabel("Y[mm]")
    ax.set_title("Powerboard Position measurements")
    ax.scatter(x_values, y_values, color = "black", marker = "o", s = 1)
    ax.grid()
    ax.invert_yaxis()

    for i, txt in enumerate(labels):
        ax.annotate(txt, (x_values[i], y_values[i]),
                    fontsize = "xx-small", rotation = 45)

    fig.savefig(plotloc/f"{plotname}_pb_posn.png")
    plt.show()


