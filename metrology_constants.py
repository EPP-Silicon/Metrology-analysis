"""
Author: Paarangat Pushkarna
Date: June 2023

Documentation of constants for R1 and R4 module metrology.

USAGE:

from metrology_constants import nominal_heights
nominal_heights["THICKNESSES"]["HYBRID"] etc.

All measurements in millimetres.
Positions and heights in coordinate system of QC document.
Origin is the top left corner of the sensor.
"""

nominal_positions = {

    "R1": {
        "H_R1H0_P1": (8.805, 58.421), #(x, y)
        "H_R1H0_P2": (108.671, 61.185),
        "H_R1H1_P1": (3.633, 20.527),
        "H_R1H1_P2": (111.731, 23.126),
        "PB_P1": (70.939, 44.617),
        "PB_P2": (107.332, 33.586)
    },

    "R4": {
        "H_R4H0_P1": (4.62227, 47.07102),
        "H_R4H0_P2": (83.92716, 49.77552),
        "H_R4H1_P1": (4.619205, 47.08994),
        "H_R4H1_P2": (83.92018, 49.72696),
        "PB_P1": (41.6643, 32.1314),
        "PB_P2": (85.27771, 15.5515)
    }
}

nominal_heights = {

    "THICKNESSES": {
        "TARGET_GLUE": 0.120,
        "HV_TAB": 0.100,
        "HYBRID": 0.270,
        "SENSOR": 0.320,
        "PB": 0.300,
        "CHIP": 0.300
    },

    "CAPACITOR_HEIGHT": 3.240,
    "SHIELD_HEIGHT": 5.840,

}

offsets = {
    "R1": (0, 0), #(x, y)
    "R4": (0, 0)
}

tolerances = {
    "GLUE_HEIGHT": 0.050,
    "POSITION": 0.250,
    "HARD_LOWER_GLUE_BOUND": 0.040
}
