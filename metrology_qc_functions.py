from pathlib import Path
import pandas as pd
import numpy as np
import argparse

from metrology_constants import (
    nominal_heights,
    nominal_positions,
    tolerances
)

from cmm_data_cleaning import (
    make_dataframe_qc_ready,
    infer_module_type
)

def setup_cli_parser() -> argparse.ArgumentParser:
    """Setup the command line interaction, help message, and inputs."""

    desc = "Execution of QA procedures specified in metrology documentation."
    parser = argparse.ArgumentParser(desc)

    iodir_desc = "Input directory containing CMM output txt files."
    parser.add_argument(
        '-i',
        '--iodir',
        type = str,
        help = iodir_desc,
        metavar='',
        required=True
    )

    return parser.parse_args()

def create_nominal_position_df(module_type: str) -> pd.DataFrame:
    """Import nominal position measurement points into pd.DataFrame.

    input: 
        module_type: "R1" or "R4"
    output:
        nominal_position_df = pd.DataFrame(
        columns = ["Location", "X_NOM[mm]", "Y_NOM[mm]"]
        )

    Imports dictionary from `metrology_constants.py' containing nominal 
    measurement points for R1 or R4 modules. Transforms dictionary into 
    pd.DataFrame for use with other dataframes. 
    
    """
    nominal_position_dict = nominal_positions[module_type]
    #create a temp column name "C0" or "C1" to rename later with .add_prefix
    
    nominal_position_df = pd.DataFrame(nominal_position_dict).T.add_prefix("C")\
    .rename_axis("Location").reset_index()
    nominal_position_df.rename(
        columns = {"C0": "X_NOM[mm]", "C1": "Y_NOM[mm]"}, inplace=True
        )

    return nominal_position_df

def calc_posn_delta(df: pd.DataFrame) -> pd.DataFrame:
    """Compute deviation from nominal positions specified in the schematics. 

    input:
        df = pd.DataFrame(
        columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location"]
        )

    output:
        combined_df = pd.DataFrame(
        columns = ["DELTA_X[mm]", "DELTA_Y[mm]", "PASSED?"]
        )

    Need to know how much Hybrids and Powerboard deviate from intended positions
    after gluing. Based on tolerances specified in QC doc, determines if 
    hybrids and powerboards have been glued within spec or not.
    """

    position_rows = df["Location"].str.contains("_P1|_P2")
    position_df = df.loc[position_rows, ["Location", "X[mm]", "Y[mm]"]]

    module_type = infer_module_type(df)
    nominal_position_df = create_nominal_position_df(module_type)


    #Can be the case that nominal position df contains more points than needed.
    #e.g. for R4M1, only one hybrid (R4H1) is present. 
    #So, we don't need points for R4H0 and R4 PB, in this case.
    
    #Below, we merge actual and nominal dataframes, discarding points we don't
    #need from the nominal dataframe. 
    combined_df = pd.merge(
        position_df,
        nominal_position_df,
        how="left",
        on=["Location",]
    )

    combined_df["DELTA_X[mm]"] = np.abs(
        combined_df["X[mm]"] - combined_df["X_NOM[mm]"]
        )

    combined_df["DELTA_Y[mm]"] = np.abs(
        combined_df["Y[mm]"] - combined_df["Y_NOM[mm]"]
        )

    combined_df["PASSED?"] = np.logical_and(
        combined_df["DELTA_X[mm]"] < tolerances["POSITION"],
        combined_df["DELTA_Y[mm]"] < tolerances["POSITION"]
        )

    cols_to_display = ["DELTA_X[mm]", "DELTA_Y[mm]", "Location", "PASSED?"]

    return combined_df.loc[:, cols_to_display].set_index("Location")

def assess_glue_layer(thickness: float) -> str:
    """Work out whether given glue tolerance is within QC spec. 

    input:
        thickness
    output:
        string specifying if thickness within spec or not.
    
    Intend for this function to be vectorised across a whole dataframe.
    Takes single values, not np.array or pd.Series/pd.DataFrame.
    """

    TARGET = nominal_heights["THICKNESSES"]["TARGET_GLUE"]
    GLUE_TOLERANCE = tolerances["GLUE_HEIGHT"]
    UPPER_BOUND = TARGET + GLUE_TOLERANCE
    LOWER_BOUND = TARGET - GLUE_TOLERANCE
    HARD_LOWER_BOUND = tolerances["HARD_LOWER_GLUE_BOUND"]

    if (thickness < UPPER_BOUND) and (thickness > LOWER_BOUND):
        return "passed"
    if (thickness < LOWER_BOUND) and (thickness > HARD_LOWER_BOUND):
        return "passed with problems"
    if (thickness < HARD_LOWER_BOUND) or (thickness > UPPER_BOUND):
        return "failed"

def agg_by_location(df: pd.DataFrame, label_str: str, agg_func=np.mean) -> pd.DataFrame:
    """Summarise height measurements by aggregating with user-specified function

    input:
        df = pd.DataFrame(
        columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location"]
        )
        label_str = r"" regex expression to choose locations.
        agg_func = function with which to aggregate (np.mean, np.amax etc.)
            defaults to np.mean.
    output:
        aggd_df = pd.DataFrame(
        columns = ["Z_AGG[mm]",], index = ["Location",]
        )

    QC specs want averaging over all points for given site, e.g. ABC_X_Y etc.
    Function enables this by doing groupby on "Location", with a user given
    function. 
    """

    rows = df["Location"].str.contains(label_str)
    selection_df = df.loc[rows, ["Location", "Z[mm]"]]

    aggd_df = selection_df.groupby("Location").agg(
        agg_height = pd.NamedAgg(column="Z[mm]", aggfunc=agg_func)
        ).rename(columns = {"agg_height":"Z_AGG[mm]"})
    #renaming because square brackets not allowed in variable names.

    return aggd_df

def calc_glue_thicknesses(df: pd.DataFrame, label_str: str, nominal_thickness: float) -> pd.DataFrame:
    """Subtract component thickness from aggregated Z measurement. 

    input:
        df = pd.DataFrame(
        columns = ["X[mm]", "Y[mm]", "Z[mm]", "Location"]
        )
        label_str = r"" regex expression to choose locations.
        nominal_thickness = constant thickness to subtract from averaged height.
    output:
        aggd_df = pd.DataFrame(
        columns = ["Z_AGG[mm]",], index = ["Location",]
        )

    Deduced from schematics that glue layer thickness between sensor and 
    component (not between chip and component) obtained by subtracting
    component thickness from measurement height. Implements this and assesses
    if result is within spec or not. 
    """

    aggd_glue_df = agg_by_location(df, label_str)

    aggd_glue_df["GLUE_THICKNESS[mm]"] = \
    aggd_glue_df["Z_AGG[mm]"] - nominal_thickness
    
    aggd_glue_df["RESULT"] = aggd_glue_df["GLUE_THICKNESS[mm]"]\
    .apply(assess_glue_layer)

    return aggd_glue_df

def calc_hyb_glue_thickness(df: pd.DataFrame) -> pd.DataFrame:

    return calc_glue_thicknesses(
        df, 
        r"ABC_[a-zA-Z0-9]+_[0-9]+|HC*_[a-zA-Z0-9]+_[0-9]+",
        nominal_heights["THICKNESSES"]["HYBRID"]
        )

def calc_pb_glue_thickness(df: pd.DataFrame) -> pd.DataFrame:

    return calc_glue_thicknesses(
        df, 
        r"PB_[0-9]+",
        nominal_heights["THICKNESSES"]["PB"]
        )

def calc_shield_height(df: pd.DataFrame) -> pd.DataFrame:

    return agg_by_location(df, r"SHIELD", agg_func=np.amax)

def exec_qc_procedures(df: pd.DataFrame) -> tuple:

    return (
        calc_posn_delta(df),
        calc_hyb_glue_thickness(df),
        calc_pb_glue_thickness(df),
        calc_shield_height(df)
        )

def main():

    cli = setup_cli_parser()
    qc_ready_df = make_dataframe_qc_ready(Path(cli.iodir))
    
    posn_delta_df, hyb_glue_df, pb_glue_df, shield_df = \
    exec_qc_procedures(qc_ready_df)

    print(posn_delta_df)
    print(hyb_glue_df)
    print(pb_glue_df)
    print(shield_df)

    return

if __name__ == "__main__":
    main()
