"""
Author: Paarangat Pushkarna
Date Created: 21/02/2023
Summary: Run script for position measurement analyses of text files produced by CMM.
To assist in metrology and quality control of R1 and R4 modules.
Does not treat bowing, only position measurements.
"""
import argparse
from pathlib import Path

from cmm_data_cleaning import (
    make_dataframe_qc_ready, 
    infer_module_type
)

from metrology_qc_functions import exec_qc_procedures

from make_metrology_db_ready import (
    make_text_file,
    make_json_file,
    make_local_log_file,
    cli_header
)

def setup_cli_parser() -> argparse.ArgumentParser:
    """Setup the command line interaction, help message, and inputs."""

    desc = "Generation of output files to push to ITK database"
    parser = argparse.ArgumentParser(desc)

    iodir_desc = "Input directory containing CMM output txt files."
    parser.add_argument(
        '-i',
        '--iodir',
        type = str,
        help = iodir_desc,
        metavar='',
        required=True
    )

    outname_desc = "Output file name. See README for details on convetion."
    parser.add_argument(
        '-n', 
        '--outname', 
        type = str, help = outname_desc, 
        metavar='', 
        required=True
    )

    return parser.parse_args()

def exec_metrology(iodir: Path, outname: str) -> None:

    header = cli_header()
    qc_ready_df = make_dataframe_qc_ready(iodir)
    qc_results = exec_qc_procedures(qc_ready_df)

    make_local_log_file(
        qc_results, iodir/f"OUT_{outname}.log"
    )

    make_text_file(
        iodir/f"OUT_{outname}.txt",
        header,
        qc_ready_df
    )
    
    make_json_file(
        qc_ready_df,
        qc_results,
        header,
        iodir/f"OUT_{outname}.json"
    )

    return

def main():

    cli = setup_cli_parser()
    exec_metrology(Path(cli.iodir), cli.outname)

    return

if __name__ == "__main__":
    main()



