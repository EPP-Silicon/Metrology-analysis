import influxdb

def openDBConnection():

    influx_connection = influxdb.InfluxDBClient(host='45.113.234.152', 
    	port=80, path='/influx', timeout=60, retries=10)
    influx_connection.switch_database('sensor_logs_weather')
    return influx_connection

def queryDB(sensor_name):

    influx_connection = openDBConnection()
    
    bind_params = {"sensor_name": sensor_name}
    result = influx_connection.query(
    	"""SELECT humidity, temperature FROM bme_environment 
    	WHERE sensor_id=$sensor_name ORDER BY DESC LIMIT 1;""", 
    	bind_params=bind_params)

    weatherLog = next(result.get_points())
    temperatureValue = weatherLog["temperature"]
    humidityValue = weatherLog["humidity"]

    return temperatureValue, humidityValue

def main():

	print(queryDB("UNIT-01"))

if __name__ == "__main__":
	main()